package com.emids.health.insurance.util;

public abstract class Constants {
	public final static String YES = "Yes";
	public final static String NO = "No";
	public final static String MALE = "Male";
	public final static String FEMALE = "Female";
	public final static String OTHER = "Other";
	public final static double MIN_PREMIUM_AMOUNT = 5000;
	public final static String ERROR_MESSAGE = "Service GateWay Failed ! Please Retry !";
}
