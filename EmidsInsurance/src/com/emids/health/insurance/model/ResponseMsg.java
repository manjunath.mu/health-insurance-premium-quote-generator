package com.emids.health.insurance.model;

public class ResponseMsg {
	private String name;
	private double amount;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = Math.round(amount);
	}

	@Override
	public String toString() {
		return "Health Insurance Premium for Mr. " + name + "  : Rs. " + amount;
	}
}
