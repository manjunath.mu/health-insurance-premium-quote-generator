package com.emids.health.insurance.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.emids.health.insurance.model.PremiumData;
import com.emids.health.insurance.model.ResponseMsg;
import com.emids.health.insurance.service.HealthInsuranceService;
import com.emids.health.insurance.util.Constants;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class HealthInsuranceController {
	@Autowired
	HealthInsuranceService healthInsuranceService;

	private final Logger logger = Logger.getLogger(this.getClass());

	@RequestMapping(value = "/calculatePremium.action", method = RequestMethod.POST)
	public @ResponseBody String calculateTotalPremium(@RequestParam String data) {
		double totalPremiumAmount = 0;
		ResponseMsg response = new ResponseMsg();
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			PremiumData premiumForm = mapper.readValue(data.toString(), PremiumData.class);
			if (premiumForm.getDailyExercise().equals(Constants.YES)) {

			}
			logger.info("Input Request :" + premiumForm.toString());
			totalPremiumAmount = healthInsuranceService.calculateTotalPremium(premiumForm);
			response.setAmount(totalPremiumAmount);
			response.setName(premiumForm.getName());
		} catch (Exception e) {
			logger.error("Error : " + e.getMessage());
			return "Error : " + Constants.ERROR_MESSAGE;

		}
		logger.info("Success : " + response.toString());
		return "Success : " + response.toString();
	}

}
