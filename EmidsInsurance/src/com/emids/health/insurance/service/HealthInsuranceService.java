package com.emids.health.insurance.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emids.health.insurance.model.PremiumData;
import com.emids.health.insurance.util.Constants;
import com.emids.health.insurance.util.Util;

@Service
public class HealthInsuranceService {
	@Autowired
	private Util util;

	public double calculateTotalPremium(PremiumData form) {
		double amountToBePaid = 0;
		// condition based on age
		amountToBePaid = totalPercentByAgeLimit(form.getAge());
		// condition based on gender
		amountToBePaid = amountByGender(form.getGender(), amountToBePaid);
		// condition based on Health and Habits
		amountToBePaid = getCostForHealthStatus(form, amountToBePaid);
		return amountToBePaid;
	}

	private double totalPercentByAgeLimit(int age) {
		double amount = 0;
		amount = age <= 40 ? util.calculatePoint(age) : util.calculatePoint(age) + (util.calculatePoint(age) * 0.2);
		return amount;
	}

	private double amountByGender(String gender, double amount) {
		double amountByAge = 0;
		if (gender.equalsIgnoreCase(Constants.MALE)) {
			amountByAge = amount + (amount * 0.02);
		} else {
			amountByAge = amount;
		}
		return amountByAge;
	}

	private double getCostForHealthStatus(PremiumData form, double amount) {
		int actualPerc = 0;
		if (form.getBloodPressure().equalsIgnoreCase(Constants.YES)) {
			actualPerc = actualPerc + 1;
		}
		if (form.getBloodSugar().equalsIgnoreCase(Constants.YES)) {
			actualPerc = actualPerc + 1;
		}
		if (form.getHypertension().equalsIgnoreCase(Constants.YES)) {
			actualPerc = actualPerc + 1;
		}
		if (form.getOverWeight().equalsIgnoreCase(Constants.YES)) {
			actualPerc = actualPerc + 1;
		}

		if (form.getAlcohol().equalsIgnoreCase(Constants.YES)) {
			actualPerc = actualPerc + 3;
		}
		if (form.getDrugs().equalsIgnoreCase(Constants.YES)) {
			actualPerc = actualPerc + 3;
		}
		if (form.getSmoking().equalsIgnoreCase(Constants.YES)) {
			actualPerc = actualPerc + 3;
		}
		if (form.getDailyExercise().equalsIgnoreCase(Constants.YES)) {
			actualPerc = actualPerc - 3;
		}
		if (actualPerc != 0) {
			amount = amount + (amount * actualPerc) / 100;
		}

		return amount;
	}
}
