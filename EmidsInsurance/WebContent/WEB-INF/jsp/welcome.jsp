<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
        <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="ISO-8859-1" />
<title>Health Insurance</title>
<link rel="stylesheet" href="css/style.css" />
</head>
<body>

	<h1 class="register-title">Insurance Premium Quote Generator</h1>
	<div class="register" align="center">
		<h3  style="color: red; font-weight: bold;" id="errorMsg"></h3>
	</div>
	<div  class="register">
		<h1	style="font-weight: bold; color: green;" id="successMsg"></h1>
	</div>

	<form  class="register" >
		
		<input type="text" class="register-input" placeholder="Name"	id="insName" autofocus name="name" required="required" />
		 <input type="number" class="register-input" placeholder="Enter your Age" name="age" id="insAge"	required="required" /> <br></br>
			<fieldset>
			<legend style="color: blue; font-weight: bold;"> Gender :</legend></fieldset>
		<div >
			<input type="radio" name="gender" value="Female" id="sex_f"		  checked="" /> 
			<label for="sex_f"  >Female</label>
			 <input type="radio" name="gender" value="Male" id="sex_m"   />
			<label for="sex_m"  >Male</label>
			<input type="radio" name="gender" value="Other" id="sex_o"   />
			<label for="sex_o"  >Other</label>
		</div>
		<br></br>
		<fieldset>
			<legend style="color: blue; font-weight: bold;">Current
				Health :</legend>

			<table>
				<tr>
					<td>Hypertension :</td>
					<td>Yes <input type="radio" name="hypertension" value="yes" id="hypertensionId" />
						No <input type="radio" name="hypertension" value="no" checked="checked"/></td>
				</tr>
				<tr>
					<td>Blood Pressure :</td>
					<td>Yes <input type="radio" name="bloodPressure" value="yes" id="bloodPressureId" />
						No <input type="radio" name="bloodPressure" value="no" checked="checked"/></td>
				</tr>
				<tr>
					<td>Blood Sugar :</td>
					<td>Yes <input type="radio" name="bloodSugar" value="yes" id="bloodSugarId" />
						No <input type="radio" name="bloodSugar" value="no" checked="checked"/></td>
				</tr>
				<tr>
					<td>Overweight :</td>
					<td>Yes <input type="radio" name="overWeight" value="yes" id="overWeightId" />
						No <input type="radio" name="overWeight" value="no" checked="checked"/></td>
				</tr>

			</table>
		</fieldset>


		<br></br>
		<fieldset>
			<legend style="color: blue; font-weight: bold;">Habits : </legend>

			<table>
				<tr>
					<td>Smocking :</td>
					<td>Yes <input type="radio" name="smoking" value="yes"  id="smokingId" /> No
						<input type="radio" name="smoking" value="no" checked="checked" /></td>
				</tr>
				<tr>
					<td>Alcohol :</td>
					<td>Yes <input type="radio" name="alcohol" value="yes" id="alcoholId"/> No
						<input type="radio" name="alcohol" value="no" checked="checked" /></td>
				</tr>
				<tr>
					<td>Daily Exercise :</td>
					<td>Yes <input type="radio" name="dailyExercise" value="yes" id="dailyExerciseId"  />
						No <input type="radio" name="dailyExercise" value="no" checked="checked" /></td>
				</tr>
				<tr>
					<td>Drugs :</td>
					<td>Yes <input type="radio" name="drugs" value="yes" id="drugsId" /> No <input
						type="radio" name="drugs" value="no" checked="checked"/></td>
				</tr>

			</table>
		</fieldset>
		<input    value="Generate Premium" class="register-button" onclick="submitForm();"/>
	</form>
	<script type="text/javascript">
	function submitForm(){
		debugger;
		if(document.getElementById("insName").value.trim()==""){
			document.getElementById("successMsg").innerHTML="";
			document.getElementById("errorMsg").innerHTML="Enter your Name";
		}else if(document.getElementById("insAge").value==""){
			document.getElementById("successMsg").innerHTML="";
			document.getElementById("errorMsg").innerHTML="Enter your Age";
		}else if(document.getElementById("insAge").value<=0){
			document.getElementById("successMsg").innerHTML="";
			document.getElementById("errorMsg").innerHTML="Enter Valid Age";
		}else{
			document.getElementById("successMsg").innerHTML="";
			document.getElementById("errorMsg").innerHTML="";
			var json_upload = "data=" + JSON.stringify({
				name : document.getElementById("insName").value,
				age:  document.getElementById("insAge").value,
				gender:document.getElementById("sex_m").checked==true?"Male":"Female",
				hypertension:document.getElementById("hypertensionId").checked==true?"Yes":"No",
				bloodPressure:document.getElementById("bloodPressureId").checked==true?"Yes":"No",
				bloodSugar:document.getElementById("bloodSugarId").checked==true?"Yes":"No",
				overWeight:document.getElementById("overWeightId").checked==true?"Yes":"No",
				smoking:document.getElementById("smokingId").checked==true?"Yes":"No",
				alcohol:document.getElementById("alcoholId").checked==true?"Yes":"No",
				dailyExercise:document.getElementById("dailyExerciseId").checked==true?"Yes":"No",
				drugs:document.getElementById("drugsId").checked==true?"Yes":"No",
			});
			
			var xmlhttp = new XMLHttpRequest(); // new HttpRequest instance 
			xmlhttp.open("POST", "calculatePremium.action");
			xmlhttp.onreadystatechange = function() {
			        if (xmlhttp.readyState>3 && xmlhttp.status==200) { 
			        	debugger;
			        	var response=xmlhttp.responseText;
			        	if(response.indexOf("Success")!=-1){
			        		document.getElementById("successMsg").innerHTML=response.substring(response.indexOf(":")+1,response.length);
			        		document.getElementById("errorMsg").innerHTML="";
			        	}else if(response.indexOf("Error")!=-1){
			        		document.getElementById("successMsg").innerHTML="";
			        		document.getElementById("errorMsg").innerHTML=response;
			        	}
			        	
			        	}
			    };
			xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
			xmlhttp.send(json_upload);
		}
		
		
	}
		
	</script>
</body>
</html>